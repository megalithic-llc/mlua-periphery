# mlua-periphery Changelog

## [0.1.9] - 2024-10-29
### Changed
- [#31](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/31) Upgrade mlua from 0.9.9 → 0.10.0

## [0.1.8] - 2024-10-19
### Changed
- [#29](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/29) Upgrade serial2 from 0.2.26 → 0.2.27
- [#28](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/28) Upgrade from Rust 1.80.0 → 1.82.0

## [0.1.7] - 2024-08-16
### Added
- [#25](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/25) Provide an example that performs i2cdetect

### Changed
- [#26](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/26) Upgrade 4 crates
- [#24](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/24) Upgrade Rust from 1.76.0 → 1.80.0

## [0.1.6] - 2024-05-11
### Changed
- [#23](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/23) Upgrade serial2 from 0.2.20 → 0.2.24

## [0.1.5] - 2024-03-09
### Changed
- [#22](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/22) Upgrade serial2 from 0.2.19 → 0.2.20
- [#20](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/20) Relocate integration tests to more idiomatic tests folder

### Fixed
- [#21](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/21) README is missing badge for armv7 support

## [0.1.4] - 2024-02-18
### Changed
- [#19](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/19) Upgrade 2 crates
- [#18](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/18) Upgrade Rust from 1.72.1 → 1.76.0

### Fixed
- [#17](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/17) Periphery error handling does not conform to lua-periphery; pcall fails to capture errors

## [0.1.3] - 2023-12-16
### Added
- [#12](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/12) Support GPIO `read_event()` fn

### Changed
- [#13](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/13) Upgrade serial2 crate from 0.2.12 → 0.2.13

## [0.1.2] - 2023-12-03
### Added
- [#11](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/11) Add badges to README for architecture and Lua VM support
- [#9](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/9) Provide an 'outdated' make target

### Changed
- [#10](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/10) Upgrade 5 crates
- [#8](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/8) Use build matrix to simplify GitLab CI config

## [0.1.1] - 2023-10-29
### Added
- [#4](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/4) Check merge requests on armv7
- [#3](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/3) Check merge requests on aarch64
- [#2](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/2) Provide a README

### Changed
- [#8](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/8) Publish tag releases to crates.io
- [#7](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/7) Test merge requests with a matrix of Lua VM versions
- [#6](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/6) Drop mlua-socket dev dependency to streamline test config

## [0.1.0] - 2023-10-07
### Added
- [#1](https://gitlab.com/megalithic-llc/mlua-periphery/-/issues/1) Support GPIO, I²C, LED, and Serial
