FEATURES ?= "lua51"

.PHONY: build
build:
	cargo build --examples --features ${FEATURES}


.PHONY: check
check:
	cargo clippy --examples --features ${FEATURES} -- --no-deps
	cargo test --examples --features ${FEATURES} -- --show-output --test-threads=1


.PHONY: fmt
fmt:
	cargo fmt --all


.PHONY: clean
clean:
	cargo clean


.PHONY: outdated
outdated:
	cargo install --locked cargo-outdated
	cargo outdated -R
