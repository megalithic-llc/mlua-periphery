use mlua::{Function, Lua};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;

    let find_components = include_str!("find_components.lua");
    let the_fn: Function = lua.load(find_components).eval()?;
    the_fn.call::<()>("/dev/i2c-1")?;
    Ok(())
}
