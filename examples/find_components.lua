local I2C = require('periphery').I2C

function find_components(device)
  io.stderr:write('     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\n')
  for i = 0, 127, 16 do
    io.stderr:write(string.format('%02x: ', i))
    for j = 0, 15 do
      local addr = i + j
      if addr < 0x03 or addr > 0x77 then
        io.stderr:write('   ')
      else
        local i2c = I2C(device, addr)
        local message = { 0, flags = I2C.I2C_M_RD }
        local ok = pcall(function()
          return i2c:transfer(addr, { message })
        end)
        if ok then
          io.stderr:write(string.format('%02x ', addr))
        else
          io.stderr:write('-- ')
        end
      end
    end
    io.stderr:write('\n')
  end
end

return find_components
