mod testsupport;

use mlua::Lua;
use std::env;
use std::error::Error;

const GPIO_TEST_WRITE: &str = "GPIO_TEST_WRITE";

#[test]
fn write_line() -> Result<(), Box<dyn Error>> {
    let value = match env::var(GPIO_TEST_WRITE) {
        Ok(value) => match value.as_str() {
            "true" => true,
            "false" => false,
            _ => return Err("Use true or false".to_string())?,
        },
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", GPIO_TEST_WRITE);
            return Ok(());
        }
    };
    let (skip, chip) = testsupport::get_gpio_chip()?;
    if skip {
        return Ok(());
    };
    let (skip, line_out) = testsupport::get_gpio_line_out()?;
    if skip {
        return Ok(());
    };
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;
    testsupport::preload(&lua)?;
    let script = r#"
        local GPIO = require('periphery.GPIO')
        local gpio = GPIO('_chip_', _line_out_, 'out')
        gpio:write(_value_)
    "#
    .replace("_chip_", &chip)
    .replace("_line_out_", &line_out)
    .replace("_value_", format!("{}", value).as_str());
    lua.load(script).exec()?;
    Ok(())
}
