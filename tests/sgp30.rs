mod testsupport;

use mlua::{Lua, Table};
use std::env;
use std::error::Error;

const SGP30_TEST: &str = "SGP30_TEST";

#[test]
fn measure() -> Result<(), Box<dyn Error>> {
    let _ = match env::var(SGP30_TEST) {
        Ok(_) => {}
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", SGP30_TEST);
            return Ok(());
        }
    };
    let (skip, device) = testsupport::get_i2c_device()?;
    if skip {
        return Ok(());
    }
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;
    testsupport::preload(&lua)?;
    let script = r#"
        local testsupport = require('testsupport')
        local I2C = require('periphery.I2C')
        local i2c = I2C('_device_')

        -- send command
        local req = {0x20, 0x32}
        i2c:transfer(0x58, { req })

        -- read response
        testsupport.sleep(0.220)
        local res = {0x00, 0x00, 0x00, flags=I2C.I2C_M_RD}
        i2c:transfer(0x58, { res })

        return res
    "#
    .replace("_device_", &device);
    let res: Table = lua.load(script).eval()?;
    let (x1, x2, x3): (String, String, String) = (res.get(1)?, res.get(2)?, res.get(3)?);
    assert_eq!(res.len()?, 6);
    eprintln!("Got {} {} {}", x1, x2, x3);
    Ok(())
}
