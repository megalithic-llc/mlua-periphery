mod testsupport;

use mlua::Lua;
use std::env;
use std::error::Error;

const GPIO_TEST_LOOPBACK: &str = "GPIO_TEST_LOOPBACK";

#[test]
fn loopback() -> Result<(), Box<dyn Error>> {
    if let Err(_e) = env::var(GPIO_TEST_LOOPBACK) {
        log::warn!("Skipping test because no {} is set", GPIO_TEST_LOOPBACK);
        return Ok(());
    }
    let (skip, chip) = testsupport::get_gpio_chip()?;
    if skip {
        return Ok(());
    };
    let (skip, line_in) = testsupport::get_gpio_line_in()?;
    if skip {
        return Ok(());
    };
    let (skip, line_out) = testsupport::get_gpio_line_out()?;
    if skip {
        return Ok(());
    };
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;
    testsupport::preload(&lua)?;
    let script = r#"
        local GPIO = require('periphery.GPIO')
        local gpio_in = GPIO({
            path='_chip_',
            line=_line_in_,
            direction='in',
            edge='both',
        })
        local gpio_out = GPIO('_chip_', _line_out_, 'out')
        gpio_out:write(true)
        assert(gpio_in:read() == true)
        gpio_out:write(false)
        assert(gpio_in:read() == false)
    "#
    .replace("_chip_", &chip)
    .replace("_line_in_", &line_in)
    .replace("_line_out_", &line_out);
    lua.load(script).exec()?;
    Ok(())
}
