mod testsupport;

use mlua::Lua;
use std::{env, error::Error};

const BITSCOPE_TEST: &str = "BITSCOPE_TEST";

#[test]
fn status() -> Result<(), Box<dyn Error>> {
    if let Err(_e) = env::var(BITSCOPE_TEST) {
        log::warn!("Skipping test because no {} is set", BITSCOPE_TEST);
        return Ok(());
    };
    let (skip, device) = testsupport::get_serial_device()?;
    if skip {
        return Ok(());
    }
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;
    testsupport::preload(&lua)?;
    let script = r#"
        local Serial = require('periphery.Serial')
        local serial = Serial('_device_', 115200)
        local timeout_ms = 50

        -- perform the command
        serial:write('=')
        assert(serial:read(1, timeout_ms) == '=')
        assert(serial:read(1, timeout_ms) == '\n')

        -- read the response line
        local line = ''
        for i = 1, 15 do
            local c = serial:read(1, timeout_ms)
            if c == nil or c == '\n' then
                break
            end
            line = line .. c
        end
        return line
    "#
    .replace("_device_", &device);
    let line: String = lua.load(script).eval()?;
    let tokens: Vec<&str> = line.trim().split(' ').collect();
    assert_eq!(tokens.len(), 5);
    let (id, ms, pwr, cur, fan) = (tokens[0], tokens[1], tokens[2], tokens[3], tokens[4]);
    eprintln!("BitScope id={} ms={} pwr={} cur={} fan={}", id, ms, pwr, cur, fan);
    Ok(())
}
