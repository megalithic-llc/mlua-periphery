mod testsupport;

use mlua::Lua;
use std::error::Error;

#[test]
fn read_line() -> Result<(), Box<dyn Error>> {
    let (skip, chip) = testsupport::get_gpio_chip()?;
    if skip {
        return Ok(());
    };
    let (skip, line_in) = testsupport::get_gpio_line_in()?;
    if skip {
        return Ok(());
    };
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;
    testsupport::preload(&lua)?;
    let script = r#"
        local GPIO = require('periphery.GPIO')
        local gpio = GPIO('_chip_', _line_in_, 'in')
        return gpio:read()
    "#
    .replace("_chip_", &chip)
    .replace("_line_in_", &line_in);
    let value: bool = lua.load(script).eval()?;
    eprintln!("GPIO chip={} line={} read()={}", chip, line_in, value);
    Ok(())
}
