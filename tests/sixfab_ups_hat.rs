mod testsupport;

use mlua::{Lua, Table};
use std::env;
use std::error::Error;

const SIXFAB_TEST: &str = "SIXFAB_TEST";

#[test]
fn get_input_voltage() -> Result<(), Box<dyn Error>> {
    if let Err(_e) = env::var(SIXFAB_TEST) {
        log::warn!("Skipping test because no {} is set", SIXFAB_TEST);
        return Ok(());
    };
    let (skip, device) = testsupport::get_i2c_device()?;
    if skip {
        return Ok(());
    }
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;
    crate::testsupport::preload(&lua)?;
    let script = r#"
        local testsupport = require('testsupport')
        local I2C = require('periphery.I2C')
        local i2c = I2C('_device_')

        -- send GetInputVoltage (0x02) command
        local req = {0xcd, 0x02, 0x01, 0x00, 0x00, 0xc8, 0x9a}
        i2c:transfer(0x41, { req })

        -- wait for device to prepare response
        testsupport.sleep(0.01)

        -- read response
        local res = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, flags=I2C.I2C_M_RD}
        i2c:transfer(0x41, { res })

        return res
    "#
    .replace("_device_", &device);
    let res: Table = lua.load(script).eval()?;
    let (datalen_hi, datalen_lo): (u8, u8) = (res.get(4)?, res.get(5)?);
    let datalen = (((datalen_hi as u16) << 8) | datalen_lo as u16) as usize;
    assert_eq!(datalen, 4);
    let (x3, x2, x1, x0): (u8, u8, u8, u8) = (res.get(6)?, res.get(7)?, res.get(8)?, res.get(9)?);
    let raw_reading = ((x3 as u32) << 24) + ((x2 as u32) << 16) + ((x1 as u32) << 8) + x0 as u32;
    let voltage = raw_reading as f64 / 1000_f64;
    eprintln!("Got input voltage {}", voltage);
    assert_ne!(voltage, 0_f64);
    Ok(())
}
