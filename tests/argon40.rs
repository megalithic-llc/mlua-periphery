mod testsupport;

use mlua::Lua;
use std::env;
use std::error::Error;

const ARGON40_SET_FAN_SPEED: &str = "ARGON40_SET_FAN_SPEED";

#[test]
fn set_fan_speed() -> Result<(), Box<dyn Error>> {
    let (skip, device) = testsupport::get_i2c_device()?;
    if skip {
        return Ok(());
    }
    let speed = match env::var(ARGON40_SET_FAN_SPEED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", ARGON40_SET_FAN_SPEED);
            return Ok(());
        }
    };
    let lua = Lua::new();
    mlua_periphery::preload(&lua)?;
    testsupport::preload(&lua)?;
    let script = r#"
        local I2C = require('periphery.I2C')
        local i2c = I2C('_device_')
        local messages = { {_speed_} }
        return i2c:transfer(0x1a, messages)
    "#
    .replace("_device_", &device)
    .replace("_speed_", &speed);
    lua.load(script).exec()?;
    Ok(())
}
