# mlua-periphery

A Rust-native implementation of [lua-periphery](https://github.com/vsergeev/lua-periphery)
for [mlua](https://crates.io/crates/mlua).

[![License](http://img.shields.io/badge/Licence-MIT-blue.svg)](LICENSE)
[![Arch](https://img.shields.io/badge/Arch-aarch64%20|%20amd64%20|%20armv7-blue.svg)]()
[![Lua](https://img.shields.io/badge/Lua-5.1%20|%205.2%20|%205.3%20|%205.4%20|%20LuaJIT%20|%20LuaJIT%205.2-blue.svg)]()

From [lua-periphery](https://github.com/vsergeev/lua-periphery):

> lua-periphery is a library for GPIO, LED, PWM, SPI, I2C, MMIO, and Serial peripheral I/O interface access
> in userspace Linux. It is useful in embedded Linux environments (including Raspberry Pi, BeagleBone, etc. platforms)
> for interfacing with external peripherals.
>
> Using Python or C? Check out the [python-periphery](https://github.com/vsergeev/python-periphery)
> and [c-periphery](https://github.com/vsergeev/c-periphery) projects.

## Installing

Add to your Rust project using one of MLua's features: [lua51, lua52, lua53, lua54, luajit, luajit52].

```shell
$ cargo add mlua-periphery --features luajit
```

## Using

```rust
use mlua::Lua;

let lua = Lua::new();
mlua_periphery::preload(&lua);
let script = r#"
    local LED = require('periphery.LED')
    local led = LED('led0')
    return led:read(), led.brightness, led.max_brightness
"#;
let (value, brightness, max_brightness): (bool, u32, u32) = lua.load(script).eval()?;
```

## Testing

An `integration_tests.rs` file exists in each submodule and demonstrates:

* Read metrics from the BMC on a [BitScope Cluster Blade](https://docs.bitscope.com/cluster-blade) via serial
* Reading the battery voltage of a [Sixfab UPS HAT](https://sixfab.com/product/raspberry-pi-power-management-ups-hat/) via I²C
* Controlling the fan speed of an [Argon Fan HAT](https://argon40.com/products/argon-fan-hat) via I²C
* Interacting with GPIO and LED pins

## Running the i2cdetect example

```shell
$ cargo run -v --example i2cdetect --features lua54

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- 1a -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         
```
