use mlua::Lua;
use std::{env, error::Error};

const SERIAL_DEVICE: &str = "SERIAL_DEVICE";

#[test]
fn constructor_1_arg_table() -> Result<(), Box<dyn Error>> {
    let device = match env::var(SERIAL_DEVICE) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", SERIAL_DEVICE);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local Serial = require('periphery.Serial')
        local serial = Serial({
            baudrate=2400,
            databits=8,
            device='_device_',
            parity="none",
            rtscts=false,
            stopbits=1,
            xonxoff=false,
        })
        return serial.baudrate, serial.databits, serial.fd, serial.parity, serial.rtscts, serial.stopbits, serial.xonxoff
    "#
        .replace("_device_", &device);
    let (baudrate, databits, fd, parity, rtscts, stopbits, xonxoff): (u32, i32, i64, String, bool, i32, bool) =
        lua.load(script).eval()?;
    assert_eq!(baudrate, 2400);
    assert_eq!(databits, 8);
    assert_ne!(fd, 0);
    assert_eq!(parity, "none");
    assert_eq!(rtscts, false);
    assert_eq!(stopbits, 1);
    assert_eq!(xonxoff, false);
    Ok(())
}

#[test]
fn constructor_2_args_device_baudrate() -> Result<(), Box<dyn Error>> {
    let device = match env::var(SERIAL_DEVICE) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", SERIAL_DEVICE);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local Serial = require('periphery.Serial')
        local serial = Serial('_device_', 4800)
        return serial.baudrate
    "#
    .replace("_device_", &device);
    let baudrate: u32 = lua.load(script).eval()?;
    assert_eq!(baudrate, 4800);
    Ok(())
}
