use crate::serial::Serial;
use mlua::Error::RuntimeError;
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, serial: &Serial, _arg: mlua::Value) -> Result<(), Error> {
    let locked_port = serial.port.lock().map_err(|err| RuntimeError(err.to_string()))?;
    Ok(locked_port.flush()?)
}
