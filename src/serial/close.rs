use crate::serial::Serial;
use mlua::Error::RuntimeError;
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, _serial: &Serial, _arg: mlua::Value) -> Result<(), Error> {
    Err(RuntimeError("NIY".to_string()))
}
