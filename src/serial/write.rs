use crate::serial::Serial;
use mlua::Error::RuntimeError;
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, serial: &Serial, arg: mlua::String) -> Result<usize, Error> {
    let arg = arg.to_str()?;
    let data = arg.as_bytes();
    let bytes_written = {
        let locked_port = serial.port.lock().map_err(|err| RuntimeError(err.to_string()))?;
        locked_port.write(data)?
    };
    Ok(bytes_written)
}
