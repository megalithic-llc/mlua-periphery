use crate::serial::Serial;
use mlua::Error::RuntimeError;
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, serial: &Serial, _arg: mlua::Value) -> Result<mlua::Number, Error> {
    let _locked_port = serial.port.lock().map_err(|err| RuntimeError(err.to_string()))?;
    Err(RuntimeError("NIY".to_string()))
}
