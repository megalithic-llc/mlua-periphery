use mlua::Lua;
use std::env;
use std::error::Error;

const LED: &str = "LED";

#[test]
fn constructor() -> Result<(), Box<dyn Error>> {
    let led = match env::var(LED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", LED);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local LED = require('periphery.LED')
        local led = LED('_led_')
    "#
    .replace("_led_", &led);
    lua.load(script).exec()?;
    Ok(())
}

#[test]
fn name_field() -> Result<(), Box<dyn Error>> {
    let led = match env::var(LED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", LED);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local LED = require('periphery.LED')
        local led = LED('_led_')
        return led.name
    "#
    .replace("_led_", &led);
    let name: String = lua.load(script).eval()?;
    assert_eq!(name, led);
    Ok(())
}

#[test]
fn read() -> Result<(), Box<dyn Error>> {
    let led = match env::var(LED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", LED);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local LED = require('periphery.LED')
        local led = LED('_led_')
        return led:read()
    "#
    .replace("_led_", &led);
    let _value: bool = lua.load(script).eval()?;
    Ok(())
}

#[test]
fn brightness_field() -> Result<(), Box<dyn Error>> {
    let led = match env::var(LED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", LED);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local LED = require('periphery.LED')
        local led = LED('_led_')
        return led.brightness
    "#
    .replace("_led_", &led);
    let _brightness: u32 = lua.load(script).eval()?;
    Ok(())
}

#[test]
fn max_brightness_field() -> Result<(), Box<dyn Error>> {
    let led = match env::var(LED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", LED);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local LED = require('periphery.LED')
        local led = LED('_led_')
        return led.max_brightness
    "#
    .replace("_led_", &led);
    let max_brightness: u32 = lua.load(script).eval()?;
    assert!(max_brightness > 0);
    Ok(())
}
