use mlua::Lua;
use std::{env, error::Error};

const I2C_DEVICE: &str = "I2C_DEVICE";

#[test]
fn constructor() -> Result<(), Box<dyn Error>> {
    let device = match env::var(I2C_DEVICE) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", I2C_DEVICE);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local I2C = require('periphery.I2C')
        local i2c = I2C('_device_')
    "#
    .replace("_device_", &device);
    lua.load(script).exec()?;
    Ok(())
}

#[test]
fn constructor_via_parent_module() -> Result<(), Box<dyn Error>> {
    let device = match env::var(I2C_DEVICE) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", I2C_DEVICE);
            return Ok(());
        }
    };
    let lua = Lua::new();
    crate::preload(&lua)?;
    let script = r#"
        local periphery = require('periphery')
        local I2C = periphery.I2C
        local i2c = I2C('_device_')
    "#
    .replace("_device_", &device);
    lua.load(script).exec()?;
    Ok(())
}
