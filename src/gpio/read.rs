use crate::gpio::configure::configure_line_request_flags;
use crate::gpio::Gpio;
use mlua::{Error, Lua, MultiValue};

pub(super) fn handle(_lua: &Lua, gpio: &Gpio, _arg: MultiValue) -> Result<bool, Error> {
    // Determine line request flags
    let line_request_flags = configure_line_request_flags(
        gpio.direction
            .lock()
            .map_err(|err| Error::RuntimeError(err.to_string()))?
            .as_str(),
    )?;

    // Lock and use the line
    let line = gpio.line.lock().map_err(|err| Error::RuntimeError(err.to_string()))?;
    let handle = line
        .request(line_request_flags, 0, "periphery:read()")
        .map_err(|err| Error::RuntimeError(err.to_string()))?;
    let value = handle.get_value().map_err(|err| Error::RuntimeError(err.to_string()))?;
    Ok(value > 0)
}
