use gpio_cdev::{EventRequestFlags, LineRequestFlags};
use mlua::Error;

pub(super) fn configure_line_request_flags(direction: &str) -> Result<LineRequestFlags, Error> {
    Ok(match direction {
        "in" => LineRequestFlags::INPUT,
        "out" => LineRequestFlags::OUTPUT,
        "low" => LineRequestFlags::OUTPUT | LineRequestFlags::ACTIVE_LOW,
        "high" => LineRequestFlags::OUTPUT,
        _ => Err(Error::RuntimeError(
            "Invalid direction; use one of [in,out,low,high]".to_string(),
        ))?,
    })
}

pub(super) fn configure_event_request_flags(edge: &str) -> Result<EventRequestFlags, Error> {
    Ok(match edge {
        "rising" => EventRequestFlags::RISING_EDGE,
        "falling" => EventRequestFlags::FALLING_EDGE,
        "both" => EventRequestFlags::BOTH_EDGES,
        _ => Err(Error::RuntimeError(
            "Invalid edge; use one of [rising,falling,both]".to_string(),
        ))?,
    })
}
