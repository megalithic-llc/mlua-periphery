use crate::gpio::configure::configure_line_request_flags;
use crate::gpio::Gpio;
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, gpio: &Gpio, arg: bool) -> Result<(), Error> {
    // Determine line request flags
    let line_request_flags = configure_line_request_flags(
        gpio.direction
            .lock()
            .map_err(|err| Error::RuntimeError(err.to_string()))?
            .as_str(),
    )?;

    // Lock and use the line
    let line = gpio.line.lock().map_err(|err| Error::RuntimeError(err.to_string()))?;
    let handle = line
        .request(line_request_flags, 0, "periphery:read()")
        .map_err(|err| Error::RuntimeError(err.to_string()))?;
    let value = match arg {
        true => 1,
        false => 0,
    };
    handle
        .set_value(value)
        .map_err(|err| Error::RuntimeError(err.to_string()))?;
    Ok(())
}
