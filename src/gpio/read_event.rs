use crate::gpio::Gpio;
use gpio_cdev::LineEvent;
use mlua::{Error, Lua, MultiValue};

pub(super) fn handle(_lua: &Lua, gpio: &Gpio, _arg: MultiValue) -> Result<LineEvent, Error> {
    // Lock and use the line event handle
    let mut line_events_guard = gpio
        .line_events
        .lock()
        .map_err(|err| Error::RuntimeError(err.to_string()))?;

    // Get the next event
    let event = line_events_guard
        .as_mut()
        .unwrap()
        .get_event()
        .map_err(|err| Error::RuntimeError(err.to_string()))?;
    Ok(event)
}
