use crate::gpio::Gpio;
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, _gpio: &Gpio, _arg: mlua::Value) -> Result<(), Error> {
    Err(Error::RuntimeError("NIY".to_string()))
}
