use mlua::Lua;
use std::{env, error::Error};

const GPIO_CHIP: &str = "GPIO_CHIP";

#[test]
fn constructor_1_arg_table() -> Result<(), Box<dyn Error>> {
    let chip = match env::var(GPIO_CHIP) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", GPIO_CHIP);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local GPIO = require('periphery.GPIO')
        local gpio = GPIO{
            bias='default',
            direction='in',
            drive='default',
            edge='none',
            inverted=true,
            label=nil,
            line=23,
            path='_chip_',
        }
        return gpio.bias, gpio.chip_label, gpio.chip_name, gpio.direction, gpio.drive, gpio.edge, gpio.inverted, gpio.label, gpio.line
    "#
        .replace("_chip_", &chip);
    let (bias, chip_label, chip_name, direction, drive, edge, inverted, label, line): (
        String,
        String,
        String,
        String,
        String,
        String,
        bool,
        String,
        u16,
    ) = lua.load(script).eval()?;
    eprintln!("chip label={} name={}", chip_label, chip_name);
    assert_eq!(bias, "default");
    assert_ne!(chip_label, "");
    assert_ne!(chip_name, "");
    assert_eq!(direction, "in");
    assert_eq!(drive, "default");
    assert_eq!(edge, "none");
    assert_eq!(inverted, true);
    assert_eq!(label, "");
    assert_eq!(line, 23);
    Ok(())
}

#[test]
fn constructor_3_args_path_line_direction() -> Result<(), Box<dyn Error>> {
    let chip = match env::var(GPIO_CHIP) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", GPIO_CHIP);
            return Ok(());
        }
    };
    let lua = Lua::new();
    super::preload(&lua)?;
    let script = r#"
        local GPIO = require('periphery.GPIO')
        local gpio = GPIO('_chip_', 23, 'in')
        return gpio.line, gpio.direction
    "#
    .replace("_chip_", &chip);
    let (line, direction): (u16, String) = lua.load(script).eval()?;
    assert_eq!(line, 23);
    assert_eq!(direction, "in");
    Ok(())
}
