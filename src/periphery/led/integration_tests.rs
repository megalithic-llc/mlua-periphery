use std::env;
use std::error::Error;

const LED: &str = "LED";

#[test]
fn read() -> Result<(), Box<dyn Error>> {
    let led = match env::var(LED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", LED);
            return Ok(());
        }
    };
    super::read(&led)?;
    Ok(())
}

#[test]
#[ignore] // because writes require root
fn write_then_read() -> Result<(), Box<dyn Error>> {
    let led = match env::var(LED) {
        Ok(ok) => ok,
        Err(_e) => {
            log::warn!("Skipping test because no {} is set", LED);
            return Ok(());
        }
    };
    super::write(&led, true)?;
    assert_eq!(super::read(&led)?, true);
    super::write(&led, false)?;
    Ok(())
}
