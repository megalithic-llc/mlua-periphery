#[cfg(test)]
pub mod integration_tests;

use std::error::Error;
use std::fs;

pub fn get_brightness(led: &String) -> Result<u32, Box<dyn Error>> {
    let path = format!("/sys/class/leds/{}/brightness", led);
    let brightness: u32 = fs::read_to_string(path)?.trim_end().parse()?;
    Ok(brightness)
}

pub fn get_max_brightness(led: &String) -> Result<u32, Box<dyn Error>> {
    let path = format!("/sys/class/leds/{}/max_brightness", led);
    let brightness: u32 = fs::read_to_string(path)?.trim_end().parse()?;
    Ok(brightness)
}

pub fn read(led: &String) -> Result<bool, Box<dyn Error>> {
    let brightness = get_brightness(led)?;
    Ok(brightness != 0)
}

pub fn set_brightness(led: &String, brightness: u32) -> Result<(), Box<dyn Error>> {
    let path = format!("/sys/class/leds/{}/brightness", led);
    Ok(fs::write(path, format!("{}\n", brightness))?)
}

pub fn write(led: &String, value: bool) -> Result<(), Box<dyn Error>> {
    match value {
        true => {
            let max_brightness = get_max_brightness(led)?;
            set_brightness(led, max_brightness)?
        }
        false => set_brightness(led, 0)?,
    };
    Ok(())
}
