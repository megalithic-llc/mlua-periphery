mod sleep;

use mlua::{Error, Lua, Table};

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let module = lua.create_table()?;
    module.raw_set("sleep", lua.create_function(sleep::handle)?)?;

    // Preload module
    let globals = lua.globals();
    let package: Table = globals.get("package")?;
    let loaded: Table = package.get("loaded")?;
    loaded.set("testsupport", module)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;
    use std::time::Instant;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::preload(&lua)?;
        lua.load("require('testsupport')").exec()?;
        Ok(())
    }

    #[test]
    fn sleep() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::testsupport::preload(&lua)?;
        let start_time = Instant::now();
        lua.load(
            r#"
                local testsupport = require('testsupport')
                testsupport.sleep(0.2)
            "#,
        )
        .exec()?;
        let elapsed = start_time.elapsed();
        assert!(elapsed.as_millis() > 190);
        assert!(elapsed.as_millis() < 400);
        Ok(())
    }
}
