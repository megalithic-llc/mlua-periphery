mod gpio;
mod i2c;
mod led;
mod periphery;
mod serial;

#[cfg(test)]
mod testsupport;

use mlua::{Error, Lua};

pub fn preload(lua: &Lua) -> Result<(), Error> {
    gpio::preload(lua)?;
    i2c::preload(lua)?;
    led::preload(lua)?;
    serial::preload(lua)?;

    // Preload root module
    let script = r#"
        local M = {
          GPIO = require('periphery.GPIO'),
          I2C = require('periphery.I2C'),
          LED = require('periphery.LED'),
          Serial = require('periphery.Serial')
        }
        package.preload['periphery'] = function() return M end
    "#;
    lua.load(script).exec()?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn load() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::preload(&lua)?;
        let table: Table = lua.load("return require('periphery')").eval()?;
        assert!(table.contains_key("GPIO")?);
        assert!(table.contains_key("I2C")?);
        assert!(table.contains_key("LED")?);
        assert!(table.contains_key("Serial")?);
        Ok(())
    }
}
